#!/usr/bin/env bash

function assertToken
{
    TOKEN=$1
    URL=$2

    response=$(curl --silent -H "Authorization: Token ${TOKEN}" ${URL})

    echo "${URL} | ${response}"

    if [[ '{"message":"DevOps test server is flying!!"}' == "${response}" ]]; then
        echo "Token: ${TOKEN} - Valid";
        return
    fi;

    echo "Token: ${TOKEN} - Invalid"
}

#==================
# Variables Parser
#==================
[[ -n $1 ]] && URL=$1 || URL="http://localhost:8000/"
[[ -n $2 ]] && LOCAL=$2 || LOCAL=""
#==================
# Docker build
#==================
if [[ -n "$LOCAL" ]]; then
    cp -r ../app ./
    docker build ./ -t oowlish-devops-engineer:dev
    docker rm -f devops-engineer || :
    docker run -d -p 8000:8000 --name devops-engineer oowlish-devops-engineer:dev
    rm -rf app
    sleep 1
fi


#==================
# Tests Execution
#==================
assertToken "SuperSecretToken" ${URL}
assertToken "NOP" ${URL}